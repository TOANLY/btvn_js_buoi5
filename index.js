// Bài 1: Quản lý tuyển sinh 
//input 
function ketQua() {
var khuVucA = 2;
var khuVucB = 1;
var khuVucC = 0.5;
var khuVuc0 = 0;
var doiTuong1 = 2.5;
var doiTuong2 = 1.5;
var doiTuong3 = 1;
var doiTuong0 = 0;
var diemChuan = document.getElementById('diemChuan').value*1;
var diemToan = document.getElementById('diemToan').value*1;
var diemVan = document.getElementById('diemVan').value*1;
var diemAnh = document.getElementById('diemAnh').value*1;
var khuVuc = document.getElementById('khuVuc').value;
var doiTuong = document.getElementById('doiTuong').value;
var khuVucDung;
var doiTuongDung;

// xử lý 


if (khuVuc === 'A') {
    khuVucDung = khuVucA * 1;
} else if (khuVuc === 'B') {
    khuVucDung = khuVucB * 1;
}else if (khuVuc === 'C') {
    khuVucDung = khuVucC *1;
}else  {
    khuVucDung = khuVuc0 *1;
}
if (doiTuong === '1') {
    doiTuongDung = doiTuong1 *1;
}else if (doiTuong === '2') {
    doiTuongDung = doiTuong2 *1;
}else if (doiTuong === '3') {
    doiTuongDung = doiTuong3 *1;
}else {
    doiTuongDung = doiTuong0 *1;
}
console.log({diemToan,diemVan,diemAnh});
console.log({khuVucDung,doiTuongDung});
tong3Mon = diemToan + diemVan + diemAnh;
diemTongKet = tong3Mon + khuVucDung + doiTuongDung;
//output
if (diemTongKet >= diemChuan) {
    document.getElementById('ketQua').innerText = 'Chúc mừng bạn đã trúng tuyển';
} else {
    document.getElementById('ketQua').innerText = 'Chúc bạn may mắn lần sau';
}
}

// BÀI 2: TÍNH TIỀN ĐIỆN
function tienDien() {
//input
var namMuoiKyDau = 500;
var namMuoiKyKe = 650;
var motTramKyKe = 850;
var motNamMuoiKyKe = 1100;
var conLai = 1300;
var soKy = document.getElementById('soKy').value*1;
var tienMotKy ;

//xử lý 

if(soKy <= 50) {
    tienMotKy = namMuoiKyDau;
} else if (50 < soKy <= 100) {
    tienMotKy = namMuoiKyKe;
} else if (100 < soKy <= 200) {
    tienMotKy = motTramKyKe;
} else if (200 < soKy <= 350) {
    tienMotKy = motNamMuoiKyKe;
} else {
    tienMotKy = conLai;
}
console.log('số ký',soKy);
var tienDien = soKy * tienMotKy;
// Output
document.getElementById('tienDien').innerText = `Tiền điện tháng này là ${tienDien}`;
}

// BÀI 3: TÍNH THUẾ THU NHẬP CÁ NHÂN 
function tinhThue() {
//input
var tongThuNhap = document.getElementById('tongThuNhap').value*1;
var nguoiPhuThuoc = document.getElementById('nguoiPhuThuoc').value*1;
var mucMot = 5;
var mucHai = 10;
var mucBa = 15;
var mucBon = 20;
var mucNam = 25;
var mucSau = 30;
var mucBay = 35;
var phanTramThue ;

//xử lý 
if(thuNhapChiuThue <= 60 ) {
    phanTramThue = mucMot ;
} else if (60 < thuNhapChiuThue <= 120) {
    phanTramThue = mucHai ; 
} else if (120 < thuNhapChiuThue <= 210) {
    phanTramThue = mucBa ;
} else if (210 < thuNhapChiuThue <= 384) {
    phanTramThue = mucBon ; 
} else if (384 < thuNhapChiuThue <= 624) {
    phanTramThue = mucNam ;
} else if (624 < thuNhapChiuThue <= 960) {
    phanTramThue = mucSau ; 
} else {
    phanTramThue = mucBay ;
}
console.log('tổng thu nhập',tongThuNhap);
console.log('người phụ thuộc',nguoiPhuThuoc);

var thuNhapChiuThue = tongThuNhap - 4 - nguoiPhuThuoc * 1.6 ;
var thueThuNhap = thuNhapChiuThue / 100 * phanTramThue;

//output 
document.getElementById('thue').innerText = `Thuế thu nhập của bạn là ${thueThuNhap} triệu đồng`;
}

// Bài 4: Tính tiền cáp 

const NHA_DAN = 'nhaDan' ;
const DOANH_NGHIEP = 'doanhNghiep';

// nhận dô các loại phí 
function phiXuLyHoaDon(loaiKhachHang) {
    phi = 0;
    if (loaiKhachHang == NHA_DAN) {
        phi = 4.5;
    } else {
        phi = 15;
    }
    return phi;
}

function phiDichVuCoBan(loaiKhachHang) {
    var soKetNoi = document.getElementById('soKetNoi').value*1;
    var phiDichVu = 0;
    if (loaiKhachHang == NHA_DAN) {
        phiDichVu = 20.5;
    } else {
        if (soKetNoi <= 10) {
            phiDichVu = 75; 
        } else {
            phiDichVu = 75 + (5 * (soKetNoi - 10));
        }
    }
    return phiDichVu;
}

function thueKenhCaoCap(loaiKhachHang) {
    var soKenhCaoCap = document.getElementById('soKenhCaoCap').value*1;
    var phiKenhCaoCap = 0;
    if (loaiKhachHang == NHA_DAN) {
        phiKenhCaoCap = 7.5 * soKenhCaoCap;
    } else {
        phiKenhCaoCap = 50 * soKenhCaoCap;
    }
    return phiKenhCaoCap ;
}



// hàm chính 
function tienCap() {
    var loaiKhachHang = document.querySelector('input[name="selector"]:checked').value;
    
    var phiXuLyHoaDon = phiXuLyHoaDon(loaiKhachHang);
    var phiDichVuCoBan = phiDichVuCoBan(loaiKhachHang);
    var thueKenhCaoCap = thueKenhCaoCap(loaiKhachHang);

    console.log({phiXuLyHoaDon, phiDichVuCoBan, thueKenhCaoCap});
    
    // tính số tiền phải trả 
    tinhTienCap = phiXuLyHoaDon + phiDichVuCoBan + thueKenhCaoCap;

    document.getElementById('Cap').innerText = `tiền cap tháng này của bạn là ${tinhTienCap} $`;
}


